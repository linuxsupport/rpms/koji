# Koji build system


Upgrade procedure:

 1. Go to [Fedora's Koji](https://koji.fedoraproject.org/koji/packageinfo?packageID=1181) and look for one of their latest builds of Koji for el8.
 2. Download and install the source RPM.
 3. Copy the source tar file and any patches to the `src/` directory of this repo.
 4. Remove the old tar file.
 5. Copy the new `koji.spec` file into this directory and edit it for CERN. In particular, change source0 and add any CERN patches. Watch out as you may need to adapt the already existing patches for the new version.
